Twister = {}
local Twister = Twister

local disableTwister = false
local registered = false

Twister.Settings = {
    auras = {
        [1] = {ability=-1, name=L"", icon=0},
        [2] = {ability=-1, name=L"", icon=0},
        [3] = {ability=-1, name=L"", icon=0},
    },
    button = 119,
    sets = {},
}
local auras = Twister.Settings.auras

local curButton = 0

Twister.activeAura = 0
Twister.activeAuraIndex = 0
Twister.auraQueue = {}

Twister.fadingAuras = {}
local fadingAuras = Twister.fadingAuras

local timeCount = 0

local auraCastTimer = 0

local function print(txt)
    EA_ChatWindow.Print(towstring(txt))
end

function Twister.SaveSet(name)
    if not Twister.Settings.sets then Twister.Settings.sets = {} end
    local set = {}
    Twister.Settings.sets[name] = set
    for i=1,3 do
        set[i] = {ability=auras[i].ability,name=auras[i].name,icon=auras[i].icon}
    end
end

function Twister.LoadSet(name)
    if not Twister.Settings.sets then Twister.Settings.sets = {} end
    local set = Twister.Settings.sets[name]
    if not set then return end
    for i=1,3 do
        auras[i] = {ability=set[i].ability,name=set[i].name,icon=set[i].icon}
    end
    Twister.OnLoad()
end

function Twister.OnLButtonDown()
    --
end

function Twister.SetActiveAura(auraNum, index)
    Twister.activeAura = auraNum
    Twister.activeAuraIndex = index
    local icon, x, y = GetIconData(auras[auraNum].icon)
    local auraWin = "TwisterAura"..auraNum
    WindowSetDimensions(auraWin, 64, 64)
    DynamicImageSetTexture(auraWin, icon, x, y)
    WindowSetOffsetFromParent(auraWin, 0, 0)
    WindowSetGameActionData(auraWin, GameData.PlayerActions.DO_ABILITY, auras[auraNum].ability, L"")
    WindowSetTintColor(auraWin, 255, 255, 255)
    WindowSetShowing(auraWin, true)
    
    for k,v in ipairs(Twister.auraQueue) do
        if v == auraNum then
            table.remove(Twister.auraQueue, k)
            break
        end
    end
    table.insert(Twister.auraQueue, auraNum)
    
    WindowSetLayer("TwisterAura"..Twister.auraQueue[1], 4)
    if Twister.auraQueue[2] then
        WindowSetLayer("TwisterAura"..Twister.auraQueue[2], 3)
        if Twister.auraQueue[3] then
            WindowSetLayer("TwisterAura"..Twister.auraQueue[3], 2)
        end
    end
    
    local icon, x, y = GetIconData(auras[Twister.auraQueue[1]].icon)
    DynamicImageSetTexture("TwisterAuraCast", icon, x, y)
end

function Twister.ClearActiveAura()
    if Twister.activeAura <= 0 then return end
    
    if not fadingAuras[Twister.activeAura] then
        WindowSetShowing("TwisterAura"..Twister.activeAura, false)
    end
    Twister.activeAura = 0
    Twister.activeAuraIndex = 0
end

function Twister.AuraFadeStart(auraNum, duration, index)
    if fadingAuras[auraNum] and fadingAuras[auraNum].expires > timeCount then
        fadingAuras[auraNum].index = index
        return
    end
    fadingAuras[auraNum] = {start=timeCount-(12-duration), expires=timeCount+duration, index=index}
    local icon, x, y = GetIconData(auras[auraNum].icon)
    DynamicImageSetTexture("TwisterAura"..auraNum, icon, x, y)
    WindowSetDimensions("TwisterAura"..auraNum, 48, 48)
    WindowSetShowing("TwisterAura"..auraNum, true)
    if Twister.activeAura == auraNum then
        Twister.ClearActiveAura()
    end
    
    -- The aura cooldown actually begins when one is *de-toggled*, not when one is cast.
    -- We'll leave a little slop time here, though.
    auraCastTimer = timeCount + 4
end

function Twister.FadingAuraEnded(auraNum)
    fadingAuras[auraNum] = nil
    if Twister.activeAura ~= auraNum then
        local scrollWidth = WindowGetDimensions("Twister") - 112
        WindowSetOffsetFromParent("TwisterAura"..auraNum, scrollWidth+64, 16)
        WindowSetTintColor("TwisterAura"..auraNum, 255, 100, 100)
    end
end

function Twister.OnEffectsUpdate(updatedEffects)
    
    if disableTwister or not updatedEffects then return end
	
	local knownActive = {}
    
    for index,data in pairs(updatedEffects) do
        if data.castByPlayer or not next(data) then
            for i=1,3 do
                if fadingAuras[i] and fadingAuras[i].index == index and not next(data) then
                    -- aura worn off
                    Twister.FadingAuraEnded(i)
                elseif auras[i].ability == data.abilityId and data.castByPlayer then
                    -- currently selected aura
                    auras[i].name = data.name
                    auras[i].icon = data.iconNum
					if Twister.activeAura ~= i then
						Twister.SetActiveAura(i, index)
					end
					knownActive[i] = true
                elseif not knownActive[i] and auras[i].icon == data.iconNum and
                    data.duration > 6 and next(data) then
                    -- lingering aura, start timer
                    Twister.AuraFadeStart(i, data.duration, index)
                end
            end
            if index == Twister.activeAuraIndex and not next(data) then
                Twister.ClearActiveAura()
            end
        end
    end
end

function Twister.OnUpdate(timePassed)
    timeCount = timeCount + timePassed
    
    if disableTwister then return end
    
    local scrollWidth = WindowGetDimensions("Twister") - 112
    
    for index,data in pairs(fadingAuras) do
        if index ~= Twister.activeAura then
            local doneness = (timeCount - data.start) / 12
            if doneness > 1 then doneness = 1 end
            WindowSetOffsetFromParent("TwisterAura"..index, (scrollWidth*doneness)+64, 16)
            if data.expires - timeCount < 2 then
                WindowSetTintColor("TwisterAura"..index, 255, 100, 100)
            end
        end
    end
    
    if auraCastTimer < timeCount and Twister.auraQueue[1] then
        WindowSetGameActionData("TwisterAuraCast", GameData.PlayerActions.DO_ABILITY,
            auras[Twister.auraQueue[1]].ability, L"")
        WindowSetTintColor("TwisterBackground", 0, 0, 100)
    else
        WindowSetGameActionData("TwisterAuraCast", GameData.PlayerActions.NONE, 0, L"")
        WindowSetTintColor("TwisterBackground", 0, 0, 0)
    end
end

function Twister.OnLoad()
    
    if GameData.Player.career.line == GameData.CareerLine.KNIGHT or
       GameData.Player.career.line == GameData.CareerLine.CHOSEN then
        disableTwister = false
        if not registered then
            registered = true
            LayoutEditor.RegisterWindow("Twister", L"Twister", L"The Twister aura display.",
                false, false, true, nil)
        end
        WindowSetShowing("Twister", true)
		if not Twister.Settings.button then Twister.Settings.button = 119 end
		TextEditBoxSetText("TwisterSettingsActionEdit", towstring(Twister.Settings.button))
		WindowSetGameActionTrigger("TwisterAuraCast", GetActionIdFromName("ACTION_BAR_"..Twister.Settings.button))
    else
        disableTwister = true
        if registered then
            registered = false
            LayoutEditor.UnregisterWindow("Twister")
        end
        WindowSetShowing("Twister", false)
		WindowSetGameActionTrigger("TwisterAuraCast", 0)
    end
    
    auras = Twister.Settings.auras
    
	timeCount = 0
	auraCastTimer = 0
	
    if disableTwister then
        WindowSetShowing("Twister", false)
    else
        WindowSetShowing("Twister", true)
    end
    
    Twister.auraQueue = {}
    for k,v in ipairs(auras) do
        WindowSetShowing("TwisterAura"..k, false)
        if v.ability > 0 then
            table.insert(Twister.auraQueue, k)
            local icon,x,y = GetIconData(v.icon)
            DynamicImageSetTexture("TwisterSettingsAura"..k, icon, x, y)
            WindowSetTintColor("TwisterSettingsAura"..k, 255, 255, 255)
        else
            DynamicImageSetTexture("TwisterSettingsAura"..k, "tint_square_twister", 0, 0)
            WindowSetTintColor("TwisterSettingsAura"..k, 80, 80, 80)
        end
    end
    if #Twister.auraQueue > 0 then
        local icon, x, y = GetIconData(auras[Twister.auraQueue[1]].icon)
        DynamicImageSetTexture("TwisterAuraCast", icon, x, y)
        WindowSetGameActionData("TwisterAuraCast", GameData.PlayerActions.DO_ABILITY,
            auras[Twister.auraQueue[1]].ability, L"")
    end
    
    fadingAuras = {}
	Twister.activeAura = 0
	Twister.activeAuraIndex = 0
    Twister.OnEffectsUpdate(GetBuffs(GameData.BuffTargetType.SELF))
end

function Twister.OnCareerUpdate()
    Twister.OnLoad()
end

-- SETTINGS WINDOW HANDLERS

function Twister.AbilityCursorSwap(flags, x, y)
    if Cursor.IconOnCursor() then
        local abilityData = GetAbilityData(Cursor.Data.ObjectId)
        Cursor.Clear()
        
        -- Check to make sure this is logical
        if abilityData == nil then return end
        if abilityData.abilityType ~= GameData.AbilityType.STANDARD then return end
        
        -- Check to see if it's an aura. (Sort of a hack, but there's not "isAura" flag)
		-- hackityhack: in general, want to check if both of these equal 4.5 - but due to rounding, it's easier to multiply by 11 and then check that the floor is 49
		-- (this is only true if the value is in between approximately 4.46 and 4.54)
        if math.floor(abilityData.cooldown*11) ~= 49 or math.floor(abilityData.reuseTimerMax*11) ~= 49 then return end
        
        -- Get the aura block we're over
        local auraNum = tonumber(SystemData.MouseOverWindow.name:match("TwisterSettingsAura([0-9]+)"))
        if not auraNum then return end
        
        auras[auraNum].ability = abilityData.id
        auras[auraNum].icon = abilityData.iconNum
        auras[auraNum].name = abilityData.name
        
        Twister.OnLoad()
    end
end

function Twister.AbilityClear(flags, x, y)
    local auraNum = tonumber(SystemData.MouseOverWindow.name:match("TwisterSettingsAura([0-9]+)"))
    if not auraNum then return end
    
    auras[auraNum].ability = -1
    auras[auraNum].icon = 0
    auras[auraNum].name = L""
    
    Twister.OnLoad()
end

function Twister.CloseSettingsWindow()
    TextEditBoxSetText("TwisterSettingsActionEdit", towstring(Twister.Settings.button))
    WindowSetShowing("TwisterSettings", false)
end

function Twister.OpenSettingsWindow()
    WindowSetShowing("TwisterSettings", true)
end

function Twister.OnButtonTextChanged()
    local button = tonumber(TextEditBoxGetText("TwisterSettingsActionEdit"))
    if not button then
        button = 119
    elseif button == 0 then
        -- ignore if box is empty
        return
    elseif button < 1 then
        button = 1
    elseif button > 120 then
        button = 120
    end
    
    Twister.Settings.button = button
    TextEditBoxSetText("TwisterSettingsActionEdit", towstring(button))
	if not disableTwister then
		WindowSetGameActionTrigger("TwisterAuraCast", GetActionIdFromName("ACTION_BAR_"..button))
	end
end

function Twister.OnButtonTextEnter()
    Twister.OnButtonTextChanged()
    Twister.CloseSettingsWindow()
    WindowAssignFocus("TwisterSettings", false)
end

function Twister.Initialize()
    for i=1,3 do WindowSetShowing("TwisterAura"..i, false) end
    
    CreateWindow("TwisterSettings", false)
    LabelSetText("TwisterSettingsTitleBarText", L"Twister")
    
    LabelSetText("TwisterSettingsActionLabel", L"Btn#:")
    
    RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "Twister.OnEffectsUpdate")
    RegisterEventHandler(SystemData.Events.LOADING_END, "Twister.OnLoad")
    RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "Twister.OnLoad")
    RegisterEventHandler(SystemData.Events.PLAYER_CAREER_LINE_UPDATED, "Twister.OnCareerUpdate")
end