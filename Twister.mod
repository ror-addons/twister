<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Twister" version="1.16" date="16/10/2010" >

		<Author name="Aiiane" email="aiiane@aiiane.net" />
		<Description text="Helper for twisting auras." />
        
        <VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
        <Dependencies>
        </Dependencies>
        
        <SavedVariables>
            <SavedVariable name="Twister.Settings" />
        </SavedVariables>
        
		<Files>
            <File name="Twister.xml" />
			<File name="Twister.lua" />
		</Files>

		
		<OnInitialize>
            <CreateWindow name="Twister" />
		    <CallFunction name="Twister.Initialize" />
		</OnInitialize>
		<OnUpdate>
            <CallFunction name="Twister.OnUpdate" />
        </OnUpdate>
		<OnShutdown/>
        
        <WARInfo>
    <Categories>
        <Category name="ACTION_BARS" />
        <Category name="BUFFS_DEBUFFS" />
        <Category name="CAREER_SPECIFIC" />
        <Category name="COMBAT" />
    </Categories>
    <Careers>
        <Career name="KNIGHT" />
        <Career name="CHOSEN" />
    </Careers>
</WARInfo>

		
	</UiMod>
</ModuleFile>
